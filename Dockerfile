FROM ubuntu:latest

ENV DEBIAN_FRONTEND=noninteractive 
RUN apt-get update -y
RUN apt-get install -y build-essential libtool curl wget\
 unzip libvsqlitepp-dev libboost-dev cmake python3 python3-pip

RUN wget\
 https://github.com/nojhan/paradiseo/archive/refs/tags/v3.0.0-beta.zip
RUN unzip v3.0.0-beta.zip
WORKDIR paradiseo-3.0.0-beta
RUN cmake -DEO_ONLY=ON -B build .
RUN make -C build install
RUN pip install pandas numpy matplotlib

WORKDIR /app
COPY . .
COPY ./data/instances/mwcp /instances

RUN cmake -B build .
RUN make -C build install

VOLUME [ "/db" ]
