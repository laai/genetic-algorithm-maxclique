//
// Created by ronaldd on 23/04/2020.
//

#include "genetic_algorithm.h"

using namespace date;

std::vector<Chrom> &GeneticAlgorithm::get_convergence() {
  return this->convergence;
}

void GeneticAlgorithm::operator()(eoPop<Chrom>& population)
{
	eoPop<Chrom> nextGen;
	Chrom _elite;
	size_t generation = 0;
	std::stringstream strstream;

	auto now = std::chrono::system_clock::now();
	strstream << now;

	std::string logfilename = std::string("Log - ") 
			+ strstream.str()
			+ std::string(".txt");
	std::ofstream filestream(logfilename);
	strstream.clear();

	// Clear convergence array
	if (!this->convergence.empty()) {
		this->convergence.clear();
	}

	// Main loop
	do {
		std::stringstream ss;

		generation++;
		// Save current best individual
		_elite = population.best_element();
		// Select for next population
		select(population, nextGen);

		// Recombination step (crossover)
		for (size_t i=0; i < (population.size()/2); i++) {
			if (rng.flip(crossoverRate)) {
				if (cross(nextGen[2*i], nextGen[2*i+1])) {
					nextGen[2*i].invalidate(); nextGen[2*i+1].invalidate();
				}
			}
		}

		// Mutation process step
		for (size_t i=0; i < nextGen.size(); i++) {
			if (rng.flip(mutationRate)) {
				if (mutate(nextGen[i]))
					nextGen[i].invalidate();
			}
		}

		// Evaluate the new generation
		population.swap(nextGen);
		for (Chrom &individual : population) {
			problem(individual);
		}

		// Recovers the elite
		auto _worseIt = population.it_worse_element();
		*_worseIt = _elite;
		auto bestElement = population.best_element();
    	ss << "[" << std::chrono::system_clock::now() << "] "
				  << "Generation " << generation << ";\t"
		          << "Fitness = " << bestElement.fitness()
				  << std::endl;
		
		std::cout << ss.str() << std::flush;
		filestream << ss.str() << std::flush;
		ss.clear();

		// Save to convergence
		this->convergence.push_back( population.best_element() );
	}
  	while (stopCriteria(population));
	nextGen.clear();

	// Show best solution
	auto bestElement = population.best_element();

	std::stringstream ss;
	ss << "----------\n";
	ss << "Best Solution" << std::endl;
	std::cout << ss.str() << std::flush;
	filestream << ss.str() << std::flush;
	ss.clear();

	int sum = std::accumulate(bestElement.begin(), bestElement.end(), 0);

	ss << "Number of vertices: " << sum 
	   << "\nTotal weight: " << bestElement.fitness() << std::endl;

	std::cout << ss.str() << std::flush;
	filestream << ss.str() << std::flush;
	ss.clear();

	filestream.close();
}
