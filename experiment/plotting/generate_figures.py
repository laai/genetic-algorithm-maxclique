import logging
from pathlib import Path
from argparse import ArgumentParser
from sqlite3 import Connection
from experiment.param_combination import ParamCombination
from experiment.plotting.functions import plot_crossover_boxplots

MCP_TABLENAME = 'maxclique_executions'
MCP_INSTANCE_FILES = [
    'C1000.9.clq',
    'C2000.5.clq',
    'C2000.9.clq',
    'p_hat1500-1.clq',
    'san1000.clq'
]

def generate_figures(params: ParamCombination, saveon: str, ext='svg') -> None:
    FORMAT = '[%(asctime)-15s] %(message)s'
    logging.basicConfig(format=FORMAT)
    logger = logging.getLogger(__name__)
    logger.warning('Configuring queries on database')

    con = Connection(params.params['db'])

    num_figures_generated = 0
    query_config = {MCP_TABLENAME: MCP_INSTANCE_FILES}

    dir = Path(saveon)
    if dir.exists() and not dir.is_dir():
        raise IOError(f'Path "{dir}" is not a directory')
    elif not dir.exists():
        dir.mkdir(exist_ok=True)
        logger.warning(f'Directory {str(dir.resolve())} was created')

    for table in query_config:
        for instance in query_config[table]:
            try:
                fig, _ = plot_crossover_boxplots(con, table, instance=instance)
                instance_no_ext = instance.replace('.', '-')
                figname = f'{table}_{instance_no_ext}.{ext}'
                if dir:
                    figpath = dir / Path(figname)
                    figname = str(figpath)
                    logger.warning(f'Saving {figname}')
                    fig.savefig(figname, transparent=True)
                    num_figures_generated += 1
            except Exception as e:
                logger.warning(f"Didn't generate figure for table='{table}' and instance_file='{instance}'. [SKIP]")
                print(e)
                pass
            pass
    con.close()
    logger.warning(f'Finished. Number of figures is {num_figures_generated}')
