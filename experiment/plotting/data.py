from sqlite3 import Connection
from dataclasses import dataclass
from typing import List

class DataSolver:
    @staticmethod
    def resolve_integers_list(convergence: str) -> List[int]:
        return [ int(value) for value in convergence.split(',') ]

    @staticmethod
    def resolve_floats_list(convergence: str) -> List[float]:
        return [ float(value) for value in convergence.split(',') ]

    @staticmethod
    def resolve_crossover_name(crossover_id: int) -> str:
        if crossover_id == 0:
            return 'Uniforme'
        elif crossover_id == 1:
            return 'Um-ponto'
        elif crossover_id > 1:
            return f'{int(crossover_id)}-pontos'
        else:
            raise ValueError('Impossible resolve the name of crossover operator')
