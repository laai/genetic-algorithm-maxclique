import argparse
import os
from pathlib import Path
import sqlite3
from string import Template
import subprocess

import numpy
from pandas import DataFrame, read_sql_query
from experiment.command import Command
from experiment.config import Config
from experiment.plotting.generate_figures import MCP_INSTANCE_FILES, generate_figures

TABLENAME = 'maxclique_executions'

PROGRAM = 'maxclique'
CROSSOVERS = [0, 1, 2]
CROSSOVER_RATE = 0.8
MUTATION_RATE = 0.0005
TOURNAMENT_RING_SIZE = 4
MAX_NUM_EVALUATIONS = 10000
POPSIZE = 50
INSTANCES = [f'/instances/{el}' for el in MCP_INSTANCE_FILES]

def crossover_name(n: int) -> str:
    if n == 0: return 'Uniform'
    elif n == 1: return 'One-point'
    else: return f'{n}-points'

parser = argparse.ArgumentParser('experiment-run')

parser.add_argument('-n', '--num-executions',
    type=int,
    default=1,
    help='Number o executions to repeat a same experiment configuration')

parser.add_argument('--name',
    type=str,
    default=f"experiment-{numpy.random.randint(10000, 99999)}",
    help='Name for identify the experiment')

params = {
    'crossover': CROSSOVERS,
    'popsize': POPSIZE,
    'xrate': CROSSOVER_RATE,
    'mrate': MUTATION_RATE,
    'ring': TOURNAMENT_RING_SIZE,
    'stop': MAX_NUM_EVALUATIONS,
    'db': '/db/results.db',
    'infile': INSTANCES
}

config = Config(params)
param_combinations = config.parse_combinations()

def generate_stats(subdir):
    sqlite_filename = f'/db/{subdir}/results.db'
    print(f'{"-"*60}\nProcessing {sqlite_filename}')
    connection = sqlite3.connect(sqlite_filename)

    t = Template(f'SELECT total_costs FROM {TABLENAME} WHERE crossover = $crossover AND instance_file = \'$instance\'')

    try:
        os.mkdir(Path(f'/db/{subdir}/csv'))
    except FileExistsError:
        print(f"Path '/db/{subdir}/csv' already exists")
        pass

    for instance in MCP_INSTANCE_FILES:
        for crossover in CROSSOVERS:
            query = t.substitute({ 'crossover': crossover, 'instance': instance })

            data = read_sql_query(query, connection, dtype={'total_costs': int})
            describe: DataFrame = data.describe()

            csv_filename = f'/db/{subdir}/csv/Stats - Crossover {crossover} - {instance}.csv'
            print(f'Generating {csv_filename}')
            describe.to_csv(csv_filename)
    
    connection.close()
    return


class Experiment:
    @staticmethod
    def run(args) -> None:
        execution = 0
        try:
            os.mkdir(f'/db/{args.name}')
        except FileExistsError:
            print(f'Directory {args.name} already exists')

        for p in param_combinations:
            p.params['db'] = f'/db/{args.name}/results.db'
            print('Combination:', p)

        for combination in param_combinations:
            execution += 1
            print('-'*60)
            print(f'Experiment {execution}: {crossover_name(combination.params["crossover"])} crossover')
            print('-'*60)
            for param in combination.params.keys():
                print(f"{param} = {combination.params[param]}")
            print('-'*60)

            command = Command(PROGRAM, params=combination)
            cmd_string = f'run {args.num_executions} {command}'
            cmd = cmd_string.split(" ")
            try:
                subprocess.call(cmd)
            except Exception as e:
                print(f'Error: {e}')

        # Plotagem de gráficos
        for combination in param_combinations:
            generate_figures(params=combination, saveon=f'/db/{args.name}')
        print(f'Figures saved in {args.name} directory.')

        return

################
# if __name__ == "__main__":
args = parser.parse_args()
Experiment.run(args)

generate_stats(args.name)
