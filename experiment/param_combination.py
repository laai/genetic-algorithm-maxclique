'''Classe ParamCombination: Combinação de parâmetros

Define uma combinação de parâmetros e exporta a string correspondente para uso
na linha de comandos com os "--{option}" e valores
'''
class ParamCombination:
    '''Classe ParamCombination: Combinação de parâmetros

    Define uma combinação de parâmetros e exporta a string correspondente para uso
    na linha de comandos com os "--{option}" e valores
    '''
    params: dict
    def __init__(self, dictP: dict):
        self.params = dictP

    def __str__(self):
        s = str()
        for option in self.params:
            s += f" --{option} {self.params[option]} "
        return s
        