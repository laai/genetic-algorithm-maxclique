'''Classe Command: Representa um comando disparável na linha de comando

É usado para criar um comando completo com um binário e uma combinação de
opções de linha de comandos.
'''

from os import system
from experiment.param_combination import ParamCombination

class Command:
  binary_name: str
  params: ParamCombination
  def __init__(self, binary: str, params=None):
    self.binary_name = binary
    self.params = params

  def __str__(self):
    return f'{self.binary_name} {self.params}'

  def run(self):
    system(str(self))