from typing import Dict, Iterable, List

from experiment.param_combination import ParamCombination

VALID_CLI_OPTIONS = [
  "infile", "db", "popsize", "stop", "crossover", "xrate", "mrate", "ring"]

class Config:
    def __init__(self, params: Dict):
        for option in params:
            if not self.is_valid_option(option):
                raise KeyError(f'Option "{option}" is invalid.')
        self.params = params

    def is_valid_option(self, optname) -> bool:
        return (optname in VALID_CLI_OPTIONS)

    def turn_all_iterable(self) -> dict:
        new_dict = dict()
        for key in self.params:
            if isinstance(self.params[key], Iterable) and not isinstance(self.params[key], str):
                new_dict[key] = self.params[key]
            else:
                new_dict[key] = [ self.params[key] ]
        return new_dict

    def parse_combinations(self) -> List[ParamCombination]:
        iter_params = self.turn_all_iterable()
        dbfile = self.params["db"]
        param_list = []

        for crossover in iter_params["crossover"]:
            for xrate in iter_params["xrate"]:
                for mrate in iter_params["mrate"]:
                    for popsize in iter_params["popsize"]:
                        for numgen in iter_params["stop"]:
                            for infile in iter_params["infile"]:
                                # Cria obj de combinação
                                pc = ParamCombination({
                                    "crossover": crossover,
                                    "xrate": xrate,
                                    "mrate": mrate,
                                    "popsize": popsize,
                                    "stop": numgen,
                                    "infile": infile,
                                    "db": dbfile
                                })
                                param_list.append(pc)
        return param_list